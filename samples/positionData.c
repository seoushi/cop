/* positionData.c */

#include <cop.h>
#include <stdio.h>
#include <malloc.h>
#include <glib.h>

#include "positionData.h"
#include "systemIds.h"



Point* point_new(float x, float y)
{
	Point* p = (Point*)malloc(sizeof(Point));
	p->x = x;
	p->y = y;
	
	return p;
}

void point_add(Point* a, Point* b)
{
	a->x += b->x;
	a->y += b->y;
}




void* positionData_addEntity(void* data, COP_Entity* entity)
{
	Point* p = point_new(0, 0);
	g_hash_table_insert(data, entity, p);

	return p;
}


void* positionData_getEntityData(void* data, COP_Entity* entity)
{
	return g_hash_table_lookup(data, (void*)entity);
}


void positionData_removeEntity(void* data, COP_Entity* entity)
{
	Point* entData = (Point*)positionData_getEntityData(data, entity);
	free(entData);
	
	g_hash_table_remove(data, entity);
}


bool positionData_hasEntity(void* data, COP_Entity* entity)
{
	return positionData_getEntityData(data, entity) != NULL;
}

void positionData_printEntity(void* data)
{
	Point* p;
	printf("\tPOSITION_DATA: ");
	
	if(data != NULL)
	{
		p = (Point*)data;
		printf("\n\t\tPosition: (%f, %f)", p->x, p->y);
	}
	
	printf("\n");
}



void positionData_init()
{
	COP_System* system = COP_system_new(POSITION_DATA,
																			positionData_addEntity,
																			positionData_removeEntity,
																			positionData_hasEntity, 
																			positionData_getEntityData,
																			positionData_printEntity);
	
	system->data = g_hash_table_new((GHashFunc)COP_entity_hash, (GEqualFunc)COP_entity_equal);

	COP_system_add(system);
}

