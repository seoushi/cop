/* vector.h */

#include <malloc.h>

#ifndef _VECTOR_H_
#define _VECTOR_H_

typedef struct
{
	unsigned int	allocatedSize;
	unsigned int length;
	void** data;
}Vector;


Vector* vector_new(unsigned int size);
void vector_destroy(Vector* vec);
void vector_resize(Vector* vec, unsigned int newSize);
void vector_push_back(Vector* vec, void* item);void* vector_get(Vector* vec, unsigned int index);
void* vector_get(Vector* vec, unsigned int index);
void vector_set(Vector* vec, unsigned int index, void* data);

#endif

