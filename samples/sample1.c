#include <stdio.h>

#include <cop.h>
#include "positionData.h"
#include "physicsSystem.h"
#include "systemIds.h"



int main(int argv, char** argc) 
{
	int i = 0;
	Point* p;
	PhysicsInfo* pi;
	COP_Entity* entities[5];
	

	/* initialize COP */
	COP_init();
	
	/* initialize some custom systems */
	positionData_init();
	physicsSystem_init();
	
	/* create 5 entities */
	for(; i < 5; i++)
	{
		COP_Entity* ent = COP_entity_new();
		entities[i] = ent;
		
		/* add some position data */
		p = (Point*)COP_system_addEntity(ent, POSITION_DATA);
		p->x = i;
		p->y = -i;
		
		/* add it to our physics system */
		pi = COP_system_addEntity(ent, PHYSICS_SYSTEM);
		pi->radius = 1;
		pi->movementVector->x = 1;
		pi->movementVector->y = 1;
	}
	
	/* print out all the entities information */
	for(i = 0; i < 5; i++)
	{
		COP_entity_print(entities[i]);
	}
	
	/* do a tick from the physics system */
	physicsSystem_update();
	
	/* print out all the entities information and see what has changed */
	for(i = 0; i < 5; i++)
	{
		COP_entity_print(entities[i]);
	}

	
	/* shutdown COP */
	COP_shutdown();
	
	return 0;
}


