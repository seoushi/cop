/* physicsSystem.h */

#include <cop.h>

#ifndef _PHYSICS_SYSTEM_H_
#define _PHYSICS_SYSTEM_H_

#include "positionData.h"

typedef struct
{
	float radius;
	Point* movementVector;
}PhysicsInfo;

/* creates a new PhysicsInfo */
PhysicsInfo* physicsInfo_new(float radius, Point* movement);


/* initializes the physics system */
void physicsSystem_init();

/* updates the physics system by a tick */
void physicsSystem_update();

#endif

