/* systemIds.h */

#ifndef _SYSTEM_IDS_H_
#define _SYSTEM_IDS_H_

/* all system IDs */
enum
{
	UNKNOWN_SYSTEM = 0,
	POSITION_DATA = 1,
	PHYSICS_SYSTEM = 2
};

#endif
