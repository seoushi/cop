/* vector.c */

#include "vector.h"


Vector* vector_new(unsigned int size)
{
	Vector* vec = (Vector*)malloc(sizeof(Vector));

	vec->allocatedSize = size;
	vec->length = 0;
	vec->data = malloc(sizeof(void*) * size);
	
	return vec;
}


void vector_destroy(Vector* vec)
{
	free(vec->data);
	free(vec);
}


void vector_resize(Vector* vec, unsigned int newSize)
{
	if(newSize < vec->length)
	{
		newSize = vec->length;
	}

	vec->data = realloc(vec->data, sizeof(void*) * newSize);
	
	vec->allocatedSize = newSize;
}


void vector_push_back(Vector* vec, void* item)
{
	if(vec->allocatedSize < (vec->length + 1))
	{
		vector_resize(vec, vec->length + 1);
	}

	vec->data[vec->length - 1] = item;
	vec->length++;
}


void* vector_get(Vector* vec, unsigned int index)
{
	return vec->data[index];
}


void vector_set(Vector* vec, unsigned int index, void* data)
{
	vec->data[index] = data;
}


