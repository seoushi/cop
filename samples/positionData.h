/* positionData.h */

#include <cop.h>

#ifndef _POSITION_DATA_H_
#define _POSITION_DATA_H_

typedef struct
{
	float x;
	float y;
}Point;

/* creates a new point */
Point* point_new(float x, float y);

/* adds point b to point a */
void point_add(Point* a, Point* b);

/* initializes and adds the position data system to the systems list */
void positionData_init();

#endif

