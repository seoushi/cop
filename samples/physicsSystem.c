/* physicsSystem.c */

#include <cop.h>
#include <stdio.h>
#include <malloc.h>
#include <glib.h>
#include <math.h>

#include "physicsSystem.h"
#include "systemIds.h"



PhysicsInfo* physicsInfo_new(float radius, Point* movement)
{
	PhysicsInfo* pi = (PhysicsInfo*)malloc(sizeof(PhysicsInfo));
	pi->radius = radius;
	pi->movementVector = movement;
	
	return pi;
}



void* physicsInfo_addEntity(void* data, COP_Entity* entity)
{
	PhysicsInfo* p = physicsInfo_new(0, point_new(0, 0));

	g_hash_table_insert(data, entity, p);
	
	return p;
}


void* physicsInfo_getEntityData(void* data, COP_Entity* entity)
{
	return g_hash_table_lookup(data, (void*)entity);
}


void physicsInfo_removeEntity(void* data, COP_Entity* entity)
{
	PhysicsInfo* entData = (PhysicsInfo*)physicsInfo_getEntityData(data, entity);
	free(entData);
	
	g_hash_table_remove(data, entity);
}



bool physicsInfo_hasEntity(void* data, COP_Entity* entity)
{
	return physicsInfo_getEntityData(data, entity) != NULL;
}


void physicsInfo_printEntity(void* data)
{
	PhysicsInfo* p;
	printf("\tPHYSICS_SYSTEM: ");
	
	if(data != NULL)
	{
		p = (PhysicsInfo*)data;
		printf("\n\t\tRadius: %f\n\t\tMovementVector: (%f, %f)", p->radius, p->movementVector->x, p->movementVector->y);
	}
	
	printf("\n");
}





void physicsSystem_init()
{
	COP_System* system = COP_system_new(PHYSICS_SYSTEM,
																			physicsInfo_addEntity,
																			physicsInfo_removeEntity,
																			physicsInfo_hasEntity, 
																			physicsInfo_getEntityData,
																			physicsInfo_printEntity);
	
	system->data = g_hash_table_new((GHashFunc)COP_entity_hash, (GEqualFunc)COP_entity_equal);

	COP_system_add(system);
}


/* checks two entities for a collision */
bool checkCollision(PhysicsInfo* pi, Point* position, PhysicsInfo* otherPi, Point* otherPosition)
{
	float diffX = otherPosition->x - position->x;
	float diffY = otherPosition->y - position->y;
	float distance = sqrt( ((diffX) * (diffX)) + ((diffY) * (diffY)));
	
	return distance < (pi->radius + otherPi->radius);
}

/* checks an entity against all other entities for a collision */
bool hasCollision(COP_Entity* entity, PhysicsInfo* pi, Point* position, COP_System* system)
{
	GHashTableIter iter;
	gpointer key, value;
	PhysicsInfo* otherPi;
	Point* otherPosition;

	g_hash_table_iter_init(&iter, system->data);

	while(g_hash_table_iter_next(&iter, &key, &value))
  {
  	/* don't test against yourself */
  	if(!COP_entity_equal(key, entity))
  	{
			otherPi = (PhysicsInfo*)value;
			otherPosition = (Point*)COP_system_getEntityData(POSITION_DATA, (COP_Entity*)key);
			
			if(checkCollision(pi, position, otherPi, otherPosition))
			{
				return true;
			}
  	}
  }
  
  return false;
}


void physicsSystem_update()
{
	COP_System* system = COP_system_getSystem(PHYSICS_SYSTEM);
	GHashTableIter iter;
	gpointer key, value;
	PhysicsInfo* pi;
	Point* position;
	Point* tempPoint = point_new(0,0);

	/* move and check for a collision for all entities */
	g_hash_table_iter_init(&iter, system->data);

	while(g_hash_table_iter_next(&iter, &key, &value))
  {
  	pi = (PhysicsInfo*)value;
  	position = (Point*)COP_system_getEntityData(POSITION_DATA, (COP_Entity*)key);
  	point_add(position, pi->movementVector);
  	
  	/* check for a collision */
  	if(hasCollision((COP_Entity*)key, pi, position, system))
  	{
  		tempPoint->x = -pi->movementVector->x;
  		tempPoint->y = -pi->movementVector->y;
  		point_add(position, tempPoint);
  	}
  }
  
  free(tempPoint);
}



