CXX = gcc

CFLAGS := -g -Wall -ansi `pkg-config --cflags glib-2.0` -Iinclude
LDFLAGS := -luuid -lg `pkg-config --libs glib-2.0`

LIBRARY = libcop.a

SRC=\
  src/entity.c\
  src/cop.c\
  src/system.c\

OBJ = $(SRC:%.c=%.o)

.cpp.o :
	$(CXX) -c $< $(CFLAGS) -o $@

all:	$(LIBRARY)

$(LIBRARY):	$(OBJ)
	ar rcs $(LIBRARY) $(OBJ)

#.PHONY : clean
clean:
	rm -f src/*.o
