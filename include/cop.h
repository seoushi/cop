/* cop.h */


#ifndef _COP_H_
#define _COP_H_

#include "entity.h"
#include "system.h"

/* initialize COP library */
void COP_init();

/* shutdown COP library */
void COP_shutdown();

#endif

