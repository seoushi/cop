/* system.h */


#ifndef _SYSTEM_HPP_
#define _SYSTEM_HPP_

#include <stdbool.h>
#include "entity.h"


typedef struct
{
	unsigned int id;
	void* (*addEntity)(void*, COP_Entity*);
	void (*removeEntity)(void*, COP_Entity*);
	bool (*hasEntity)(void*, COP_Entity*);
	void* (*getEntityData)(void*, COP_Entity*);
	void (*printEntityData)(void*);
	void* data;
}COP_System;


/* Creates a new system */
COP_System* COP_system_new(	int id,
														void* (*addEntity)(void*, COP_Entity*),
														void (*removeEntity)(void*, COP_Entity*),
														bool (*hasEntity)(void*, COP_Entity*),
														void* (*getEntityData)(void*, COP_Entity*),
														void (*printEntityData)(void*));

/* adds a system to the system list */
void COP_system_add(COP_System* system);

/* gets a specified system */
COP_System* COP_system_getSystem(unsigned int systemId);

/* remove a system from the system list */
void COP_system_remove(COP_System* system);

/* removes an entity from all systems */
void COP_system_removeEntityFromAll(COP_Entity* entity);

/* adds an entity to a system, returns the entity data */
void* COP_system_addEntity(COP_Entity* entity, unsigned int systemId);

/* removes an entity from a system */
void COP_system_removeEntity(unsigned int systemId, COP_Entity* entity);

/* tells if a system is handling an entity */
bool COP_system_hasEntity(unsigned int systemId, COP_Entity* entity);

/* gets data for a an entity */
void* COP_system_getEntityData(unsigned int systemId, COP_Entity* entity);

/* prints out a system's data for an entity */
void COP_system_printEntityData(unsigned int systemId, COP_Entity* entity);



#endif

