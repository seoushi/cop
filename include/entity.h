/* entity.h */


#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <stdbool.h>
#include <uuid/uuid.h>

/* stores information on an entity */
typedef struct
{
	uuid_t id;
}COP_Entity;

/* creates a new entity */
COP_Entity* COP_entity_new();

/* frees the entity memory and removes it from all systems*/
void COP_entity_destroy(COP_Entity* entity);

/* returns if an entity exists or not */
bool COP_entity_doesExist(COP_Entity* entity);

/* returns a comparison between two entities */
int COP_entity_compare(COP_Entity* a, COP_Entity* b);

/* returns if too entities are equal */
bool COP_entity_equal(COP_Entity* a, COP_Entity* b);

/* hashes an entity */
unsigned int COP_entity_hash(COP_Entity* entity);

/* prints and entity and all of it's related information */
void COP_entity_print(COP_Entity* entity);

#endif

