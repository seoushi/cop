/* cop.c */

#include "cop.h"
#include "entity.h"
#include <stdio.h>


void COP_entity_init();
void COP_entity_shutdown();

void COP_system_init();
void COP_system_shutdown();



void COP_init()
{
	COP_entity_init();
	COP_system_init();
}


void COP_shutdown()
{
	COP_entity_shutdown();
	COP_system_shutdown();
}

