/* entity.cpp */


#include "entity.h"

#include <malloc.h>
#include <glib.h>

#include "system.h"

GTree* COP_getEntities();
GHashTable* COP_getSystems();


COP_Entity* COP_entity_new()
{
	COP_Entity* entity = (COP_Entity*)malloc(sizeof(COP_Entity));

	uuid_generate(entity->id);

	/* make sure the uuid is unique */
	while(COP_entity_doesExist(entity))
	{
		uuid_generate(entity->id);
	}

	/* add it to the tree */
	g_tree_insert(COP_getEntities(), entity, (void*)1);
	
	return entity;
}


void COP_entity_destroy(COP_Entity* entity)
{
	if(COP_entity_doesExist(entity))
	{
		COP_system_removeEntityFromAll(entity);
		g_tree_remove(COP_getEntities(), entity);	
	}
	
	free(entity->id);
	free(entity);
}


bool COP_entity_doesExist(COP_Entity* entity)
{
	return g_tree_lookup(COP_getEntities(), entity) != 0;
}


int COP_entity_compare(COP_Entity* a, COP_Entity* b)
{
	return uuid_compare(a->id, b->id);
}

bool COP_entity_equal(COP_Entity* a, COP_Entity* b)
{
	return COP_entity_compare(a,b) == 0;
}


unsigned int COP_entity_hash(COP_Entity* entity)
{
	unsigned int result = 0;
	int i = 0;
	
	for(;i < 16; i++)
	{
		result += entity->id[i];
		result = result << 2;
	}
	
	return result;
}


void COP_entity_print(COP_Entity* entity)
{
	GHashTableIter iter;
	gpointer key, value;

	printf("Entity: %u\n", COP_entity_hash(entity));
	
	/* print each system's data */
	g_hash_table_iter_init(&iter, COP_getSystems());

	while(g_hash_table_iter_next(&iter, &key, &value))
  {
		COP_system_printEntityData(((COP_System*)value)->id, entity);
  }
}


/*********Internal functions************/


/* Used for comparing values in a tree */
gint COP_entityTreeCompare(gconstpointer  a, gconstpointer  b)
{
	return COP_entity_compare((COP_Entity*) a, (COP_Entity*) b);
}

/* get's all entities */
GTree* COP_getEntities()
{
	static GTree* entities;
	
	if(!entities)
	{
		entities = g_tree_new(COP_entityTreeCompare);
	}
	
	return entities;
}


/* clears the entity tree, should only be called from the main shutdown method */
void COP_entity_shutdown()
{
	GHashTableIter iter;
	gpointer key, value;
	
	/* remove entity from each system */
	g_hash_table_iter_init(&iter, COP_getSystems());

	while(g_hash_table_iter_next(&iter, &key, &value))
  {
		COP_system_removeEntityFromAll((COP_Entity*)value);
  }

	g_tree_destroy(COP_getEntities());
}


/* initializes the entity tree, should only be called from the main init method */
void COP_entity_init()
{
	/* get the entities so it is initialized */
	COP_getEntities();
}

