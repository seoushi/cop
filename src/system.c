/* system.c */

#include <malloc.h>
#include <glib.h>

#include "system.h"



GHashTable* COP_getSystems();




COP_System* COP_system_new(	int id,
														void* (*addEntity)(void*, COP_Entity*),
														void (*removeEntity)(void*, COP_Entity*),
														bool (*hasEntity)(void*, COP_Entity*),
														void* (*getEntityData)(void*, COP_Entity*),
														void (*printEntityData)(void*))
{
	COP_System* system = (COP_System*)malloc(sizeof(COP_System));

	system->id = id;
	system->addEntity = addEntity;
	system->removeEntity = removeEntity;
	system->hasEntity = hasEntity;
	system->getEntityData = getEntityData;
	system->printEntityData = printEntityData;
	system->data = NULL;

	return system;
}


void COP_system_add(COP_System* system)
{
	g_hash_table_insert(COP_getSystems(), (void*)system->id, system);
}


COP_System* COP_system_getSystem(unsigned int systemId)
{
	return (COP_System*)g_hash_table_lookup(COP_getSystems(), (void*)systemId);
}


void COP_system_remove(COP_System* system)
{
	g_hash_table_remove(COP_getSystems(), (void*)system->id);
}


void COP_system_removeEntityFromAll(COP_Entity* entity)
{
	GHashTableIter iter;
	gpointer key, value;

	/* remove enity from all systems */
	g_hash_table_iter_init(&iter, COP_getSystems());

	while(g_hash_table_iter_next(&iter, &key, &value))
  {
		COP_system_removeEntity(((COP_System*)value)->id, entity);
  }
}


void* COP_system_addEntity(COP_Entity* entity, unsigned int systemId)
{
	COP_System* system = NULL;
	system = (COP_System*)g_hash_table_lookup(COP_getSystems(), (void*)systemId);
	
	if(!system)
	{
		return NULL;
	}
	
	return system->addEntity(system->data, entity);
}


void COP_system_removeEntity(unsigned int systemId, COP_Entity* entity)
{
	COP_System* system = NULL;
	system = (COP_System*)g_hash_table_lookup(COP_getSystems(), (void*)systemId);
	
	if(!system)
	{
		return;
	}
	
	system->removeEntity(system->data, entity);
}


bool COP_system_hasEntity(unsigned int systemId, COP_Entity* entity)
{
	COP_System* system = NULL;
	system = (COP_System*)g_hash_table_lookup(COP_getSystems(), (void*)systemId);
	
	if(!system)
	{
		return false;
	}
	
	return system->hasEntity(system->data, entity);
}


void* COP_system_getEntityData(unsigned int systemId, COP_Entity* entity)
{
	COP_System* system = NULL;
	system = (COP_System*)g_hash_table_lookup(COP_getSystems(), (void*)systemId);
	
	if(!system)
	{
		return NULL;
	}
	
	return system->getEntityData(system->data, entity);
}


void COP_system_printEntityData(unsigned int systemId, COP_Entity* entity)
{
	COP_System* system = NULL;
	void* data = NULL;

	system = (COP_System*)g_hash_table_lookup(COP_getSystems(), (void*)systemId);
	
	if(!system)
	{
		return;
	}

	data = system->getEntityData(system->data, entity);
	
	system->printEntityData(data);
}


/***** Internal Functions *****/


/* gets the full list of systems */
GHashTable* COP_getSystems()
{
	static GHashTable* systems = NULL;
	
	if(!systems)
	{
		systems = g_hash_table_new(g_direct_hash, g_direct_equal);
	}
	
	return systems;
}


/* shuts down all systems */
void COP_system_shutdown()
{
	GHashTableIter iter;
	gpointer key, value;

	/* free all systems */
	g_hash_table_iter_init(&iter, COP_getSystems());

	while(g_hash_table_iter_next(&iter, &key, &value))
  {
		free((COP_System*)value);
  }

	g_hash_table_destroy(COP_getSystems());
}

/* initializes all systems */
void COP_system_init()
{
	/* call it once so it is initalized */
	COP_getSystems();
}


